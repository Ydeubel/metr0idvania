﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour

{
    public float CDseconds = 1;
    float cooldown;
    int maxAmmo = 50;
    int ammo;
    public Transform FirePoint;
    public GameObject BulletPrefab;
    public float nextFire;
    public float fireRate;

    void Update()
    {
       if (fireRate == 0 && Input.GetMouseButton(0))
        {
            Shoot();
        }
        else
        {
            if (Input.GetKey(KeyCode.Space) && Time.time > nextFire && fireRate > 0)
            {
               
                if (ammo > 0)
                {   
                    nextFire = Time.time + fireRate;
                    Shoot();
                    ammo--; 
                }
                if (ammo == 0)
                {   
                    if (cooldown > Time.time)
                    {   
                        cooldown = Time.time + CDseconds;
                    }
                }
            }
        }
        if (Time.time > cooldown && ammo == 0)
        {   
            ammo = maxAmmo;
        }

    }

    void Shoot()
    {
        Instantiate(BulletPrefab, FirePoint.position, FirePoint.rotation);
    }
}
