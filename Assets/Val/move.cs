﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move : MonoBehaviour
{
    public GameObject p1;
    public GameObject p2;
    public bool onP1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A)) //switch la cam entre les 2 persos lors de l'appui de la Spacebar
        {
            onP1 = !onP1;
            
        }

        if (!onP1)
        {
            SwitchCam2();
            p1.SetActive(false);
            p2.SetActive(true);
        }

        if (onP1)
        {
            SwitchCam1();
            p1.SetActive(true);
            p2.SetActive(false);
        }
    }

    void SwitchCam2() //Switch sur P2
    {
        transform.position = new Vector3(p2.transform.position.x,p2.transform.position.y,-10);
    }
    
    
    void SwitchCam1() //Switch sur P1
    {
        transform.position = new Vector3(p1.transform.position.x,p1.transform.position.y,-10);
    }
    
}
